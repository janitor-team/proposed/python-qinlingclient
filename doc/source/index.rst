Python Qinling Client
=====================

The Python Qinling Client (python-qinlingclient) is a command-line client for
the OpenStack Function as a Service.

Getting Started
---------------

.. toctree::
   :maxdepth: 2

   Project Overview <readme>
   install/index
   contributor/index

Usage
-----

.. toctree::
   :maxdepth: 2

   cli/index

Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`
