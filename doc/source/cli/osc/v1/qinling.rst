Using Qinling CLI extensions to OpenStack Client
================================================

=======
runtime
=======

.. autoprogram-cliff:: openstack.function_engine.v1
    :command: runtime *

========
function
========

.. autoprogram-cliff:: openstack.function_engine.v1
    :command: function *

===
job
===

.. autoprogram-cliff:: openstack.function_engine.v1
    :command: job *

=======
webhook
=======

.. autoprogram-cliff:: openstack.function_engine.v1
    :command: webhook *
